"use strict";
/* tslint:disable */
/* eslint-disable */
/**
 * OpenAPI definition
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: v0
 *
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
Object.defineProperty(exports, "__esModule", { value: true });
var axios_1 = require("axios");
var api_1 = require("./api/api");
var https = require("https");
var configuration_1 = require("./api/configuration");
var fs_1 = require("fs");
var basePath = "https://192.168.10.215/r1/OPTIC/GOB/GOB00001/GP-GDE";
//const basePath = "http://localhost:8080/v2-api"
var configuration2 = new configuration_1.Configuration({
    basePath: basePath,
});
var httpsAgent = new https.Agent({
    cert: (0, fs_1.readFileSync)("/mnt/disk2/trabajo/XROAD-INENCO-Certificados-Autenticacion/inenco.xroad.crt"),
    key: (0, fs_1.readFileSync)("/mnt/disk2/trabajo/XROAD-INENCO-Certificados-Autenticacion/inenco.xroad.key"),
    rejectUnauthorized: false,
});
var headers = {
    'X-Road-Client': "OPTIC/GOB/GOB00001/GP-OPTIC"
};
var filePath = "/home/walace47/tmp/hola-mundo";
var ambiente = "cap";
var usuario = "JSTACCO";
var sistema = "TEST";
//const ticketPecas = "21Z7PFF5POWEIZ577R552JBE2UCHBF";
var loginController = new api_1.LoginControllerApi(configuration2);
loginController.createAuthenticationToken(ambiente, { username: "JSTACCO", password: "Joan123" }, { httpsAgent: httpsAgent, headers: headers }).then(function (res) {
    console.log((res.data));
    headers = {
        //'Authorization': `Bearer ${res?.data?.jwt}`,
        'Authorization': "Basic ".concat(Buffer.from("JSTACCO:Joan123").toString('base64')),
        'X-Road-Client': "OPTIC/GOB/GOB00001/GP-OPTIC"
    };
    /* const headers: HTTPHeaders = {
         'Authorization': `Pecas ${ticketPecas}`
     };*/
    var configuration = new configuration_1.Configuration({
        basePath: basePath
    });
    var consultarExpedientesControllerApi = new api_1.ConsultarExpedientesControllerApi(configuration);
    var generarTareaApi = new api_1.GenerarTareaControllerApi(configuration);
    var generarExpedienteApi = new api_1.GenerarExpedienteControllerApi(configuration);
    var consultarDocumentoApi = new api_1.ConsultarDocumentoControllerApi(configuration);
    var generarDocumentoApi = new api_1.GenerarDocumentoControllerApi(configuration);
    var vincularDocumentoApi = new api_1.VincularDocumentoControllerApi(configuration);
    var paseExpedienteApi = new api_1.GenerarPaseControllerApi(configuration);
    var generarExpedienteReq = {
        motivo: "Un motivo de test",
        sistema: sistema,
        trata: "EXPEDIENTE",
        descripcion: "test"
    };
    var generarDocumentoReq = {
        acronimo: "IFI",
        archivo: ((0, fs_1.readFileSync)(filePath).toString("base64")),
        referencia: "una referencia",
        sistemaOrigen: sistema,
        tipoArchivo: "txt",
        embebidos: [{
                archivo: ((0, fs_1.readFileSync)(filePath).toString("base64")),
                nombre: "emebido1.txt"
            }],
        destinatarios: ["JSTACCO"]
    };
    var revisarGedoReq = {
        acronimo: "IFI",
        archivo: ((0, fs_1.readFileSync)(filePath).toString("base64")),
        referencia: "Una referencia de prueba WS",
        tipoArchivo: "txt",
        usuarioReceptor: usuario
    };
    var confeccionarGedoReq = {
        acronimo: "IFI",
        usuarioReceptor: "JSTACCO",
        destinatarios: ["JSTACCO"],
        enviarCorreoReceptor: true,
        meensajeADestinatario: "test",
        recibirAvisoDeFirma: true,
        mensaje: "mensaje de prueba",
    };
    var firmarGedoReq = {
        acronimo: "IFI",
        archivo: ((0, fs_1.readFileSync)(filePath).toString("base64")),
        referencia: "Una referencia de prueba WS",
        destinatarios: ["JSTACCO"],
        tipoArchivo: "txt",
        firmantes: [usuario],
    };
    var test = function () { return __awaiter(void 0, void 0, void 0, function () {
        var confeccionDocumento, revisarDocumento, firmarDocumento, generarDocumento, caratulaExpediente, vincularDocumento, paseResponse, consultaDocuProceso, consultaDocuSade, error_1, _a, data, status_1, header;
        return __generator(this, function (_b) {
            switch (_b.label) {
                case 0:
                    _b.trys.push([0, 10, , 11]);
                    return [4 /*yield*/, generarTareaApi.confeccionarGEDO(ambiente, confeccionarGedoReq, { httpsAgent: httpsAgent, headers: headers })];
                case 1:
                    confeccionDocumento = _b.sent();
                    console.log("Confeccionar documento " + JSON.stringify(confeccionDocumento === null || confeccionDocumento === void 0 ? void 0 : confeccionDocumento.data));
                    return [4 /*yield*/, generarTareaApi.revisarGEDO(ambiente, revisarGedoReq, { httpsAgent: httpsAgent, headers: headers })];
                case 2:
                    revisarDocumento = _b.sent();
                    console.log("revisar documento " + JSON.stringify(revisarDocumento === null || revisarDocumento === void 0 ? void 0 : revisarDocumento.data));
                    return [4 /*yield*/, generarTareaApi.firmarGEDO(ambiente, firmarGedoReq, { httpsAgent: httpsAgent, headers: headers })];
                case 3:
                    firmarDocumento = _b.sent();
                    console.log("firma documento " + JSON.stringify(firmarDocumento === null || firmarDocumento === void 0 ? void 0 : firmarDocumento.data));
                    return [4 /*yield*/, generarDocumentoApi.crearDocumento(ambiente, generarDocumentoReq, { httpsAgent: httpsAgent, headers: headers })];
                case 4:
                    generarDocumento = _b.sent();
                    console.log("generar documento " + JSON.stringify(generarDocumento === null || generarDocumento === void 0 ? void 0 : generarDocumento.data));
                    return [4 /*yield*/, generarExpedienteApi.generarExpedienteInterno(ambiente, generarExpedienteReq, { httpsAgent: httpsAgent, headers: headers })];
                case 5:
                    caratulaExpediente = _b.sent();
                    console.log("Generar caratula " + JSON.stringify(caratulaExpediente === null || caratulaExpediente === void 0 ? void 0 : caratulaExpediente.data));
                    return [4 /*yield*/, vincularDocumentoApi.vincularDocumento(caratulaExpediente.data.nroExpediente, ambiente, { documentos: [generarDocumento.data.numero], sistema: sistema }, { httpsAgent: httpsAgent, headers: headers })];
                case 6:
                    vincularDocumento = _b.sent();
                    console.log("Vincular" + JSON.stringify(vincularDocumento === null || vincularDocumento === void 0 ? void 0 : vincularDocumento.data));
                    return [4 /*yield*/, paseExpedienteApi.generarPasePersona(ambiente, {
                            motivo: "pase",
                            sistema: sistema,
                            usuarioDestino: "MGIGLIO",
                            codigoExpediente: caratulaExpediente.data.nroExpediente
                        }, { httpsAgent: httpsAgent, headers: headers })];
                case 7:
                    paseResponse = _b.sent();
                    console.log(JSON.stringify(paseResponse === null || paseResponse === void 0 ? void 0 : paseResponse.data));
                    return [4 /*yield*/, consultarDocumentoApi.consultarDocumentoByProceso(firmarDocumento.data.processId, ambiente, { httpsAgent: httpsAgent, headers: headers })];
                case 8:
                    consultaDocuProceso = _b.sent();
                    console.log("Detalle documento proceso " + JSON.stringify(consultaDocuProceso === null || consultaDocuProceso === void 0 ? void 0 : consultaDocuProceso.data));
                    return [4 /*yield*/, consultarDocumentoApi.consultarDocumentoBySADE(generarDocumento.data.numero, ambiente, { httpsAgent: httpsAgent, headers: headers })];
                case 9:
                    consultaDocuSade = _b.sent();
                    console.log("Detalle documento numero " + JSON.stringify(consultaDocuSade === null || consultaDocuSade === void 0 ? void 0 : consultaDocuSade.data));
                    return [3 /*break*/, 11];
                case 10:
                    error_1 = _b.sent();
                    _a = error_1 === null || error_1 === void 0 ? void 0 : error_1.response, data = _a.data, status_1 = _a.status, header = _a.header;
                    // console.log({data,status,header});
                    // error?.response?.json()?.then(e => console.log(e))
                    throw (error_1);
                case 11: return [2 /*return*/];
            }
        });
    }); };
    test().then(function (e) { return e; }).catch(function (e) {
        if (e instanceof axios_1.AxiosError) {
            // console.log(e?.response);
            var _a = e === null || e === void 0 ? void 0 : e.response, data = _a.data, status_2 = _a.status;
            console.log({ data: data, status: status_2, message: e.message });
            //console.log({status:e?.response?.status, data:e?.response?.data})
        }
    });
}).catch(function (e) {
    //console.log(e)
    if (e instanceof axios_1.AxiosError) {
        //console.log(e?.response);
        var _a = e === null || e === void 0 ? void 0 : e.response, data = _a.data, status_3 = _a.status;
        console.log({ data: data, status: status_3, message: e.message });
        //console.log({status:e?.response?.status, data:e?.response?.data})
    }
});
//# sourceMappingURL=index.js.map