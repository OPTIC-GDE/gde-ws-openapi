"use strict";
var __createBinding = (this && this.__createBinding) || (Object.create ? (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    var desc = Object.getOwnPropertyDescriptor(m, k);
    if (!desc || ("get" in desc ? !m.__esModule : desc.writable || desc.configurable)) {
      desc = { enumerable: true, get: function() { return m[k]; } };
    }
    Object.defineProperty(o, k2, desc);
}) : (function(o, m, k, k2) {
    if (k2 === undefined) k2 = k;
    o[k2] = m[k];
}));
var __exportStar = (this && this.__exportStar) || function(m, exports) {
    for (var p in m) if (p !== "default" && !Object.prototype.hasOwnProperty.call(exports, p)) __createBinding(exports, m, p);
};
Object.defineProperty(exports, "__esModule", { value: true });
/* tslint:disable */
/* eslint-disable */
__exportStar(require("./Archivo"), exports);
__exportStar(require("./ArchivoDeTrabajoResponse"), exports);
__exportStar(require("./AuthenticationRequest"), exports);
__exportStar(require("./AuthenticationRequestWithToken"), exports);
__exportStar(require("./AuthenticationResponse"), exports);
__exportStar(require("./CampoFormularioDetalleResponse"), exports);
__exportStar(require("./CamposFormularioResponse"), exports);
__exportStar(require("./ComplexData"), exports);
__exportStar(require("./ConsultaDocumento"), exports);
__exportStar(require("./ConsultarDocumentoPdfResponse"), exports);
__exportStar(require("./ConsultarTiposDocumentoResponse"), exports);
__exportStar(require("./DetalleExpedienteResponse"), exports);
__exportStar(require("./Documento"), exports);
__exportStar(require("./DocumentoMetadataResponse"), exports);
__exportStar(require("./ExpedienteResponse"), exports);
__exportStar(require("./GenerarDocumentoDTO"), exports);
__exportStar(require("./GenerarExpedienteDTO"), exports);
__exportStar(require("./GenerarExpedienteResponse"), exports);
__exportStar(require("./GenerarPasePersonaDTO"), exports);
__exportStar(require("./GenerarPaseReparticionDTO"), exports);
__exportStar(require("./GenerarPaseSectorDTO"), exports);
__exportStar(require("./GenerarTareaConfeccionarDTO"), exports);
__exportStar(require("./GenerarTareaFirmarDTO"), exports);
__exportStar(require("./GenerarTareaRevisarDTO"), exports);
__exportStar(require("./GetAllDataResponse"), exports);
__exportStar(require("./HistorialResponse"), exports);
__exportStar(require("./ItemComponenteDetalleResponse"), exports);
__exportStar(require("./Pase"), exports);
__exportStar(require("./ResponseExternalConsultaNumeroSade"), exports);
__exportStar(require("./ResponseExternalGenerarDocumento"), exports);
__exportStar(require("./ResponseExternalGenerarTarea"), exports);
__exportStar(require("./ResponseMetadata"), exports);
__exportStar(require("./ResponseTipoDocumento"), exports);
__exportStar(require("./VincularDocumentoDTO"), exports);
//# sourceMappingURL=index.js.map